$(window).on('load', function() {
	
/**
 *	MAIL
 */
	$( document ).ajaxSuccess( function() {
		$( '.modal' ).modal( 'hide' );
		$( '#sec-send' )
			.find( 'h5' ).text( 'Success' ).end()
			.find( 'p' ).text( 'Success' ).end()
			.modal( 'show' );
	});
	$( document ).ajaxError( function( data, data1, data2 ) {
		let responseText = JSON.parse( data1.responseText );
		$( 'input, textarea', '#modal-letter' ).removeClass( 'red-alarm' );
		$( responseText.messages.trim().split( ' ' ).join() ).addClass( 'red-alarm' );
	});
	$( 'button.btn', '#social' ).click( function() {
		$( 'input, textarea', '#modal-letter' ).removeClass( 'red-alarm' );
	});
	$( 'button.btn', '#modal-letter.modal' ).click( function() {
		let el$ = $( '#modal-letter' );
		
		$( 'input', el$ ).removeClass( 'red-alarm' );

		$.ajax({
			type: 'POST',
			dataType: 'json',
			data: {
				act: 1,	// custom call
				letter_name: $( '#leter-name', el$ ).val(),
				letter_email: $( '#leter-email', el$ ).val(),
				letter_message: $( '#letter-text', el$ ).val(),
			},
			url: 'mail.php'
		});
		
	});
	
	
/**
 *	animation
 */
    var el_rd$ = $( '.item-heading-small' ),
		el_soci$ = $( 'a', 'footer #social' ),
		started_small = false,
		started_soci = false;
		
    el_rd$
		.addClass( 'animated' )
		.eq(0).addClass( 'fadeOutLeft' )
		.end().eq(1).addClass( 'fadeOutRight' );
		
	el_soci$.addClass( 'zoomOut animated' );
	
    $( window ).scroll( function() {
		var cPos;
        if ( !started_small ) {
			cPos = el_rd$.offset().top;                    
			if ( cPos < ( $(window).scrollTop() * 2) ) {
				started_small = true;				
				el_rd$.eq(0).removeClass( 'fadeOutLeft' ).addClass( 'fadeInLeft' );
				el_rd$.eq(1).removeClass( 'fadeOutRight' ).addClass( 'fadeInRight' );
			}
        }
        if ( !started_soci ) {
			cPos = el_soci$.offset().top;                    
			if ( cPos < ( $(window).scrollTop() * 1.5) ) {
				started_soci = true;
				
				el_soci$.each ( function( ind ) {
					var eli$ = $(this);					   
					setTimeout( function() {
						eli$.removeClass( 'zoomOut' ).addClass( 'zoomIn' );
					}, ind * 500 );
				});
			}
        }
    });
});