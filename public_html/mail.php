<?php
define( 'TEST_MAIL', 'pastushenko.vik@gmail.com' );

	$errors = array();
	
	switch ( $_POST[ 'act' ] ) {
		case 1: //Custom Call
			$subject = 'SoftGroup letter';
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: <test@pva>' . "\r\n";
			
			if ( empty( $_POST[ 'letter_name' ] ) ) {
				$errors[ messages ] .= '#leter-name';
			} else {
				$msg = 'Name: ' .  $_POST[ 'letter_name' ] . "<br>";
			}
			if ( empty( $_POST[ 'letter_email' ] ) ) {
				$errors[ messages ] .= ' #leter-email';
			} else {
				$msg .= 'e-mail: ' .  $_POST[ 'letter_email' ] . "<br>";
			}
			if ( empty( $_POST[ 'letter_message' ] ) ) {
				$errors[ messages ] .= ' #letter-text';
			} else {
				$msg .= $_POST[ 'letter_message' ] . "<br>";
			}
			break;
	}
	
	if ( count( $errors ) == 0 ) {
        header('Content-Type: application/json; charset=UTF-8');
		mail( TEST_MAIL, $subject, $msg, $headers);
        print json_encode( $errors );		
    } else {
        header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die( json_encode( $errors ) );		
    }
?> 