<?php
	require( 'header.php' );
?>
	<div id="index-page" class="container-fluid">
		<div class="container">
			<!-- .row Large Item -->
			<div class="row">
				<a name="quick-link-one"></a>
				<div class="col-md-12">
					<div class="item-heading">
						<div class="img-div">
							<img src="img/larger-item.png" alt="Item">
						</div>
						<p class="item-titl">
							Larger item heading
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat. Fusce a pharetra lectus. Quisque auctor pellentesque tortor, ac convallis erat rutrum in. Donec ultrices dignissim risus, id suscipit massa. Sed mollis lectus non iaculis rutrum. Sed fringilla ornare tincidunt. Donec ut turpis venenatis, lacinia metus vel, vehicula sem. Suspendisse lacinia pretium viverra. In feugiat placerat viverra. Donec sit amet sem erat. 
						</p>
						<button class="btn" type="button">Details</button>
					</div>
				</div>
			</div>
			<!-- /.row Large Item -->
			
			<!-- .row Small Item -->
			<div id="small-item-row" class="row">
				<div class="col-12 bortop"><div></div></div>
				<a name="quick-link-two"></a>
				<div class="col-md-6">
					<div class="item-heading-small">
						<div>
							<div class="img-div">
								<img src="img/item-1.png" alt="Item">
							</div>
							<p class="item-titl">
								Small item
							</p>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat.
							</p>
						</div>
						<button class="btn" type="button">Details</button>
					</div>
				</div>
				<div class="col-md-6">
					<div class="item-heading-small">
						<div>
							<div class="img-div">
								<img src="img/item-2.png" alt="Item">
							</div>
							<p class="item-titl">
								Small item
							</p>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat. Fusce a pharetra lectus. Quisque auctor pellentesque tortor. 
							</p>
						</div>
						<button class="btn" type="button">Details</button>
					</div>
				</div>
			</div>
			<!-- /.row Small Item -->

			<!-- .row Photo -->
			<a name="quick-link-three"></a>
			<div id="photo-row" class="row">
				<div class="col-12 bortop"><div></div></div>
				<div class="col-md-4">
					<div class="item-heading">
						<div class="img-div">
							<img src="img/add-fhoto.png" alt="Add Photo">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="item-heading">
						<div class="img-div">
							<img src="img/add-fhoto.png" alt="Add Photo">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="item-heading">
						<div class="img-div">
							<img src="img/add-fhoto.png" alt="Add Photo">
						</div>
					</div>
				</div>
			</div>
			<!-- /.row Photo -->
			
			<!-- .row Blog Post -->
			<a name="quick-link-four"></a>
			<div id="blog-post-row" class="row">
				<div class="col-12 bortop"><div></div></div>
				<div class="col-md-12">
					<div class="item-heading">
						<p class="item-titl">
							Blog post title one
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat. Fusce a pharetra lectus. Quisque auctor pellentesque tortor, 
						</p>
						<button class="btn" type="button">Details</button>
					</div>
				</div>
			</div>
			<div id="blog-post-row" class="row">
				<div class="col-12 bortop"><div></div></div>
				<div class="col-md-12">
					<div class="item-heading">
						<p class="item-titl">
							Blog post title two
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat. Fusce a pharetra lectus. 
						</p>
						<button class="btn" type="button">Details</button>
					</div>
				</div>
			</div>
			<!-- /.row Blog Post -->
			
		</div>
	</div>

<?php
	require( 'footer.php' );
?>
