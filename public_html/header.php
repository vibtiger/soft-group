<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8>
	<meta http-equiv= "X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>Soft Group Test</title>	
 	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">
	
	<link rel="shortcut icon" href="">
	<link rel="stylesheet" href= "../test/css/animate.css">
	<link rel="stylesheet" href= "../test/css/bootstrap.min.css">
	<link rel="stylesheet" href= "../test/css/style.css">
	
</head>

<body>
	<header>
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-5">
						<a class="navbar-brand" href="/">
							<img src="img/logo.png" alt="SOFT GROUP">
						</a>
					</div>
					<div class="ml-auto col-sm-12 col-md-6">
						<p class="p-bold">
							High perfomance software development
						</p>
					</div>
				</div>
			</div>
		</div>
	</header>
