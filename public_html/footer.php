	<footer>
		<div class="container-fluid">
			<div id="social" class="container">
				<div class="row">
					<div class="col-sm-12 col-md-4">
						<p class="item-titl">
							Stay connected
						</p>
					</div>
					<div class="col-sm-12 col-md-8">
						<a href="#" target="_blank">
							<i class="fab fa-twitter"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fab fa-facebook-f"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fas fa-rss"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fab fa-pinterest"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fab fa-google-plus-g"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fab fa-dribbble"></i>
						</a>
						<a href="#" target="_blank">
							<i class="fab fa-linkedin-in"></i>
						</a>
						<a href="#" target="_blank">
							<img src="img/flikr.png">
							<!-- <i class="fab fa-flickr fa-inverse"></i> -->
						</a>
					</div>
				</div>				
				<div class="row">
					<div class="ml-auto col-auto mr-auto">
						<button class="btn" type="button"  data-toggle="modal" data-target="#modal-letter">
							Send letter
						</button>
					</div>
				</div>
			</div>
			<div class="container">
				<div id="quick-link-row" class="row">
					<div class="col-sm-12 col-md-7">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis justo sit amet nibh rhoncus feugiat. Fusce a pharetra lectus. Quisque auctor pellentesque tortor, ac convallis erat rutrum in.
						</p>
					</div>
					<div class="ml-auto col-sm-12 col-md-3">
						<ul>
							<li>
								<a href="#quick-link-one">Quick Link One</a>
							</li>
							<li>
								<a href="#quick-link-two">Quick Link Two</a>
							</li>
							<li>
								<a href="#quick-link-three">Quick Link Three</a>
							</li>
							<li>
								<a href="#quick-link-four">Quick Link Four</a>
							</li>
						</ul>
					</div>
				</div>
				<div id="copyright-row" class="row">
					<div class="col-12 bortop"><div></div></div>
					<div class="col-sm-12 col-md-7">
						<p>
							Copyright 2015 <span>SoftGroup</span>. All Rights Reserved.
						</p>
					</div>
					<div class="ml-auto col-sm-12 col-md-4">
						<p>View Online  |  Unsubscribe</p>
					</div>
				</div>
			</div>
	</footer>


<!-- MODAL -->
	<!-- Send letter -->
	<div class="modal" id="modal-letter" tabindex="-1" role="dialog" aria-labelledby="modal-letter-label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title item-titl" id="modal-letter-label">Send letter</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="leter-name">Name</label>
							<input type="text" class="form-control" id="leter-name" name="name">
						</div>
						<div class="form-group">
							<label for="leter-email">Email</label>
							<input type="email" class="form-control" id="leter-email" name="email" placeholder="example@email.ex">
						</div>
						<div class="form-group">
							<label for="letter-text">Example textarea</label>
							<textarea class="form-control" id="letter-text" name="message" rows="3"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn">Submit</button>
						</div>
					</form>					
				</div>
			</div>
		</div>
	</div>

	<!-- SMS online -->
	<div id="sec-send" class="modal" tabindex="-1" role="dialog" aria-labelledby="ss-label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title item-titl" id="ss-label"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p></p>
				</div>
			</div>
		</div>
	</div>

	<script src="../test/js/jquery-3.3.1.min.js"></script>
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script src="../test/js/bootstrap.min.js"></script>
	<script src="../test/js/custom.js"></script>
</body>
</html>
